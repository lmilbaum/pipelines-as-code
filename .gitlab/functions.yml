###############################################################################
#                           Shell functions                                   #
###############################################################################
.check_artifacts: |
  check_link() {
    curl --output /dev/null --silent --head --fail "$1"
  }
  set -x
  if [[ "${IMPORT_IMAGE}" == "yes"  ]]; then
    exit 0
  fi
  if [[ "$BUILD_FORMAT" == "img" ]]; then
    BUILD_FORMAT="raw"
  fi
  # Now we compress all teh images, so we add the .xz extension
  BUILD_FORMAT="${BUILD_FORMAT}.xz"
  if [[ -n "${S3_UPLOAD_DIR}" ]]; then
    S3_UPLOAD_PREFIX="${S3_UPLOAD_DIR}"
  elif [[ "${SAMPLE_IMAGE}" == "yes" ]]; then
    S3_UPLOAD_PREFIX="sample-images"
  else
    S3_UPLOAD_PREFIX="non-sample-images"
  fi
  BASE_URL="${CI_REPOS_ENDPOINT}/${WORKSPACE_ID}/${S3_UPLOAD_PREFIX}/${IMAGE_KEY}"
  IMAGE_URL="${BASE_URL}.${BUILD_FORMAT}"
  CHECKSUM_URL="${BASE_URL}.${BUILD_FORMAT}.sha256"
  check_link "$IMAGE_URL"
  check_link "$CHECKSUM_URL"
  cat <<-EOF > "build_info.html"
  <b>Download Image&#58;</b> <a href="${IMAGE_URL}">${IMAGE_KEY}.${BUILD_FORMAT}</a><br>
  <b>Download Checksum Data&#58;</b> <a href="${CHECKSUM_URL}">${IMAGE_KEY}.${BUILD_FORMAT}.sha256"</a><br>
  EOF

.testing-farm-request: |
  # In Testing Farm the aarch64 composes have the '-aarch64' appended to the end.
  # It'll be removed in the future and we won't need to do this.
  if [[ "${ARCH}" == "aarch64" ]] && [[ "${STREAM}" == "upstream" ]]; then
      TF_COMPOSE="${TF_COMPOSE}-aarch64"
  fi

  if [[ "${ACTION}" == "TEST" ]]; then
      TF_COMPOSE=${IMAGE_KEY}
  fi

  # This bash magic is to handle arch specific tf pools
  TF_POOL_VAR="TF_POOL_${ARCH}"

  # Compose the query for calling the Testing Farm API
  cat <<EOF > request.json
  {
    "api_key": "${TF_API_KEY}",
    "test": {
      "fmf": {
      "url": "${CI_REPO_URL}",
      "ref": "${CI_REF}",
      "name": "${TMT_PLAN}"
      }
    },
    "environments": [
     {
     "arch": "${ARCH}",
     "os": {"compose": "${TF_COMPOSE}"},
     "pool": "${!TF_POOL_VAR}",
     "variables": {
       "ARCH": "${ARCH}",
       "UUID": "${WORKSPACE_ID}",
       "REPO_URL": "${REPO_URL}",
       "REVISION": "${REVISION}",
       "CUSTOM_IMAGES_REPO": "${CUSTOM_IMAGES_REPO}",
       "CUSTOM_IMAGES_REF": "${CUSTOM_IMAGES_REF}",
       "OS_VERSION": "${OS_VERSION}",
       "OS_PREFIX": "${OS_PREFIX}",
       "OS_OPTIONS": "${BUILD_OS_OPTIONS}",
       "BUILD_TYPE": "${BUILD_TYPE}",
       "IMAGE_NAME": "${IMAGE_NAME}",
       "IMAGE_TYPE": "${IMAGE_TYPE}",
       "IMAGE_KEY": "${IMAGE_KEY}",
       "BUILD_FORMAT": "${BUILD_FORMAT}",
       "SAMPLE_IMAGE": "${SAMPLE_IMAGE}",
       "TEST_IMAGE": "${TEST_IMAGE}",
       "IMPORT_IMAGE": "${IMPORT_IMAGE}",
       "SET_IMAGE_SIZE": "${SET_IMAGE_SIZE}",
       "IMAGE_SIZE": "${IMAGE_SIZE}",
       "DISTRO_NAME": "${TEST_DISTRO_NAME}",
       "S3_BUCKET_NAME": "${S3_BUCKET_NAME}",
       "BUILD_TARGET": "${BUILD_TARGET}",
       "SSH_KEY": "${SSH_KEY}",
       "SSL_VERIFY": "${SSL_VERIFY}",
       "AWS_REGION": "${AWS_DEFAULT_REGION}",
       "AWS_TF_REGION": "${AWS_TF_REGION}",
       "AWS_CKI_REGION": "${AWS_CKI_REGION}",
       "CI_REPOS_ENDPOINT": "${CI_REPOS_ENDPOINT}",
       "PRODUCT_REPOS_ENDPOINT": "${PRODUCT_REPOS_ENDPOINT}",
       "S3_UPLOAD_DIR": "${S3_UPLOAD_DIR}",
       "STREAM": "${STREAM}",
       "PACKAGE_SET": "${PACKAGE_SET}",
       "PRODUCT_BUILD_PREFIX": "${PRODUCT_BUILD_PREFIX}",
       "PIPELINE_REPO": "${PIPELINE_REPO}",
       "SAMPLE_IMAGES_REPO": "${SAMPLE_IMAGES_REPO}",
       "OSBUILD_VERSION": "${OSBUILD_VERSION}",
       "ARTIFACTS_DATA_URL": "${ARTIFACTS_DATA_URL}",
       "KERNEL_NVR": "${KERNEL_NVR}"
       },
     "secrets": {
       "AWS_ACCESS_KEY_ID": "${AWS_ACCESS_KEY_ID}",
       "AWS_SECRET_ACCESS_KEY": "${AWS_SECRET_ACCESS_KEY}",
       "AWS_TF_ACCOUNT_ID": "${AWS_TF_ACCOUNT_ID}",
       "AWS_CKI_ACCOUNT_ID": "${AWS_CKI_ACCOUNT_ID}",
       "DOWNSTREAM_COMPOSE_URL": "${DOWNSTREAM_COMPOSE_URL}"
       }
     }
     ]
  }
  EOF

  # Do the API query to Testing Farm with the data from above
  curl --silent ${TF_ENDPOINT} \
      --header "Content-Type: application/json" \
      --data @request.json \
      --output response.json

  # Show the response, but hide the secrets values
  jq 'del(.environments |. [] | .secrets)' response.json

  ID=$(jq -r '.id' response.json)
  set +x
  echo "Wait until the job finished at the Testing Farm"
  while true; do
      rm -f response.json
      curl --silent --output response.json "${TF_ENDPOINT}/${ID}"
      STATUS=$(jq -r '.state' response.json)
      if [[ "$STATUS" == "complete" ]] || [[ "$STATUS" == "error" ]]; then
          echo ; echo "Finished"
          break
      fi
      echo -n "."
      sleep 30
  done

  # Check the tests result
  RESULT=$(jq -r '.result.overall' response.json)
  echo "Result: $RESULT"

  # Even in failure, there are artifacts and a pipeline.log
  ARITFACTS_URL=$(jq -r '.run.artifacts //""' response.json)

  # Show the artifacts URL in the log
  if [[ -n "$ARITFACTS_URL" ]]; then
      echo "Testing Farm artifacts: $ARITFACTS_URL"
      echo "Testing Farm pipeline log: $ARITFACTS_URL/pipeline.log"
      if [[ "${ACTION}" == "TEST" ]]; then
          sleep 30
          mkdir tests_results || true
          cd tests_results
          # Sanitize the JOB NAME to remove characters like / [ and ]
          # It also remove the initial /plans/, to make it shorter and more meaninful
          JOB_NAME=$(echo ${CI_JOB_NAME/\/plans\//} | tr -d '[:blank:]|\[|\]' | tr -s '/' '-')
          curl --silent --output "${JOB_NAME}-results.html" "${ARITFACTS_URL}/"
          curl --silent --output "${JOB_NAME}-results-junit.xml" "${ARITFACTS_URL}/results-junit.xml"
      fi
  fi

  # If the result is an error, there is no report to show and means that something
  # was wrong with the call or there was an internal error from Testing Farm.
  # It does not mean that the tests failed.
  if [[ "$RESULT" == "error" ]]; then
      echo "Testing Farm has returned an error:"
      jq -r '.result.summary' response.json
      exit 10
  fi
