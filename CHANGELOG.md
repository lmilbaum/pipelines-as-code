# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [Unreleased]

### Added

* Renovate precommit
  ([!7](https://gitlab.com/CentOS/automotive/coffre/pipelines-as-code/-/merge_requests/7))
* Renovate presets
  ([!6](https://gitlab.com/CentOS/automotive/coffre/pipelines-as-code/-/merge_requests/6))
* Use centos/automotive/coffre Container Registry
  ([!5](https://gitlab.com/CentOS/automotive/coffre/pipelines-as-code/-/merge_requests/5))
